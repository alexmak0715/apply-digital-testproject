import React from "react";
import classnames from "classnames/bind";

import styles from "./header.module.scss";

const cx = classnames.bind(styles);

const Header = () => {
  return (
    <div className={cx("container")}>
      <div className={cx("container_title")}>
        HN Feed
      </div>
      <div className={cx("container_subtitle")}>
        We &lt;3 hacker news!
      </div>
    </div>
  );
};

export default Header;
