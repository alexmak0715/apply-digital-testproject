# Apply Digital Test Project

## Introduction

This is a Full-stack Dockerized App.

### Stack

- React.js
- Nest.js
- MongoDB Atlas
- Docker

## Prerequisites

Make sure you have the below installed on your machine.

- [x] **Docker** : https://docs.docker.com/engine/install/
- [x] **Docker-Compose** : https://docs.docker.com/compose/install/
- [x] **Nest** : https://nestjs.com/


## Client

Client has been created with create-react-app and located in `./front-end`

#### Development

In develpoment mode the client will be run in a container built with `./front-end/Dockerfile.dev` and will be exposed on port 3000, with docker volumes every change thats been saved will be reflected within the running container.

#### Production

In production mode the client build will be created and will run in a container built with `./front-end/Dockerfile`.
The client build/static-files will be exposed on port 80.

## Server

Server is located in `./back-end` using nest.js.

#### Development

In development mode the server will run in a container built with `./back-end/Dockerfile.dev`.
and will be exposed on port 5000 to the "outside" world, with docker volumes, every change thats been saved will be refelected within the running container.

#### Production

In production mode the server will run in a container built with `./back-end/Dockerfile`.
and be exposed on port 5000 only to the docker composer internal services within the same network.

## Docker compose

### Development

To establish a development environment, simply run the following command from the project root folder.

```bash
sudo docker-compose --file docker-compose-dev.yml up
```

On save changes in client and server, containers will be automatically updated, no need to restart any servers.
</br>

### Production

To establish production environment, simply run the following command from the root folder.

```bash
sudo docker-compose up
```

This will creates build for both server and client, will serve client build on port 80 and will communicate with server on port 5000.

## Good to know

- To drop the use of sudo run the folowing command in your terminal

```bash
sudo usermod -aG docker $USER
```

- If you are making changes within the dockerfiles you will need to rebuild them, for that add the --build flag to the docker compose up command.